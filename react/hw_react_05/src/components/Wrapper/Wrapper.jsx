import React from 'react';
import './style.scss';

const Wrapper = ({ children }) => {
  return (
    <main>
      <section className="container">
        {children}
      </section>
    </main>
  );
}

export default Wrapper