import React, { useReducer } from 'react';
import Wrapper from './components/Wrapper';
import StepOne from './containers/StepOne';
import StepTwo from './containers/StepTwo';
import StepThree from './containers/StepThree';
import StepFour from './containers/StepFour';
import StepFive from './containers/StepFive';
import './App.scss';

const initialState = {
  step: 1,
  user: {},
}

export const Context = React.createContext({});

const reducer = (state, action) => {
  switch (action.type) {
    case 'NEXT':
      if (state.step >= 5) {
        return { ...state, step: 5 }
      }
      return {
        ...state,
        step: state.step + 1,
        user: action.payload
      }
    case 'PREVIOUS':
      if (state.step < 2) {
        return { ...state, step: 1 }
      }
      return {
        ...state,
        step: state.step - 1
      }
    default:
      return state
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  console.log(state)

  return (
    <div className="wrapper">
      <Context.Provider value={{ state, dispatch }}>
        <Wrapper>
          <h1 className="step">Step {state.step} of 5</h1>
        </Wrapper>
        {state.step === 1 && <StepOne />}
        {state.step === 2 && <StepTwo />}
        {state.step === 3 && <StepThree />}
        {state.step === 4 && <StepFour />}
        {state.step === 5 && <StepFive />}
      </Context.Provider>
    </div>
  );
}

export default App;
