import React, { useState } from 'react';
import Modal from '../../Modal/Modal';
import './User.css';

const User = ({ user }) => {

  const [isOpenModal, setOpenModal] = useState(false);
  const handleOpenModal = () => {
    setOpenModal(!isOpenModal);
  }

  return (
    <>
      <article className="user" onClick={handleOpenModal}>
        <div className="user__inner">
          <div className="user__photo">
            <img src={user.picture} alt="user" />
          </div>
          <div className="user__info">
            <h3 className="user__name">{user.name}</h3>
            <p className="user__age">Age: {user.age}</p>
            <p className="user__gender">Gender: {user.gender}</p>
            <p className="user__balance">Balance: {user.balance}</p>
          </div>
        </div>
      </article>
      {isOpenModal && <Modal user={user} handleOpenModal={handleOpenModal}/>}
    </>
  );
}

export default User;