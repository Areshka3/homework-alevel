import React from 'react';
import User from './User/User';
import './Users.css';

const Users = ({ users }) => {

  let usersList = users.map(user => <User key={user._id} user={user} />);

  return (
    <section className="users">
      <div className="container">
        <div className="users__list">
          {usersList}
        </div>
      </div>     
    </section>
  );
}

export default Users;