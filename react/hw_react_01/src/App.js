import React, { useState } from 'react';
import { userData } from './userData';
import Users from './components/Users/Users';
import Header from './components/Header/Header';
import customSort from './sort';
import './App.css';

function App() {

  const [users, setUsers] = useState(userData);

  const handleSerchUsers = (value) => {
    const result = userData.filter(user => user.name.toLocaleLowerCase().includes(value.toLocaleLowerCase()));
    setUsers(result);
  }

  const handleSortUsers = (value) => {
    setUsers(customSort(userData, value));
  }

  const handleReset = () => {
    setUsers(userData);
  }

  return (
    <div className="wrapper">
      <Header handleSerchUsers={handleSerchUsers} handleSortUsers={handleSortUsers} handleReset={handleReset} />
      <Users users={users} />
    </div>
  );
}

export default App;
